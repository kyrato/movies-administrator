import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import Overview from "./overview_page/Overview";
import myData from './movieDB/movies';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<App />, div);
  ReactDOM.unmountComponentAtNode(div);
});

it('should show a table without crashing', function () {
  const div = document.createElement('div');
  ReactDOM.render(<Overview movies={myData} />, div);
  ReactDOM.unmountComponentAtNode(div);
});