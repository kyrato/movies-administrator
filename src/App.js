import React from 'react';
import { Row } from 'react-bootstrap';
import './App.css';
import Overview from "./overview_page/Overview";
import AdminView from "./admin_view/AdminView";
import Footer from "./footer/Footer";
import myData from './movieDB/movies';

export default class App extends React.Component{

  constructor(props, context) {
    super(props, context);
    this.showAdminView = this.showAdminView.bind(this);
    this.updateMovies = this.updateMovies.bind(this);

    /**
     * Component states
     * @type {{show: boolean}}
     */
    this.state = {
      movies: [],
        showAdmin: true,
    };
  }

  showAdminView(){
      this.setState({
          showAdmin: !this.state.showAdmin,
      })
  }

    updateMovies(movies){
        this.setState({
            movies: movies,
        })
    }

  componentDidMount() {
    this.setState({
      movies: myData,
    });
  }

  render(){
    return (
        <div className="myContainer" >
          <Row className="overviewHeader" >
            <h1 className="titles">Frontend Excercise: Admin my movies</h1>
          </Row>
            {this.state.showAdmin && <Overview movies={this.state.movies} showAdminView={this.showAdminView}/>}
            {!this.state.showAdmin && <AdminView movies={this.state.movies}
                                                 showAdminView={this.showAdminView} updateMovies={this.updateMovies}/>}
          <Footer />
        </div>
    );
  }
}
