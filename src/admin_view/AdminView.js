import React from 'react';
import { Table, Row, Col, Button, Modal, Form } from "react-bootstrap"
import '../App.css';

export default class Overview extends React.Component {

    constructor(props, context) {
        super(props, context);

        this.handleShow = this.handleShow.bind(this);
        this.handleClose = this.handleClose.bind(this);
        this.createMovie = this.createMovie.bind(this);
        this.deleteMovie = this.deleteMovie.bind(this);
        this.searchMovie = this.searchMovie.bind(this);
        this.onChange = this.onChange.bind(this);

        /**
         * Component states
         * @type {{show: boolean}}
         */
        this.state = {
            showMovieModal: false,
            newMovie: {},
            moviesSearch: []
        };
    }

    searchMovie(event){
        event.preventDefault();
        let title = event.target.value;
        let movies = this.props.movies;
        let filteredMovies = movies.filter((movie)=>{
            if(movie.title.toLowerCase().includes(title.toLowerCase())){
                return movie;
            }
        });
        this.setState({
            moviesSearch: filteredMovies,
        });
    }

    createMovie(){
        let movies = this.props.movies;
        movies.push(this.state.newMovie);

        this.setState({
            moviesSearch: movies,
        });
        this.props.updateMovies(movies);

    }

    deleteMovie(event){
        let idx = event.target.id;
        let movies = this.props.movies;
        idx = parseInt(idx.substr(idx.length - 1));

        //  Erase item
        movies.splice(idx, 1);

        this.setState({
            moviesSearch: movies,
        });
        this.props.updateMovies(movies);
    }

    /**
     * Show Modal
     */
    handleShow(){
        this.setState({
            showMovieModal: !this.state.showMovieModal,
        });
        // console.log(this.state.show);
    }

    /**
     * Hide Modal
     */
    handleClose(){
        this.setState({
            showMovieModal: !this.state.showMovieModal,
        });
        // console.log(this.state.show);
    }

    /*
     Changing props value
     */
    onChange(event){
        event.preventDefault();
        let newMovie = this.state.newMovie;
        newMovie[event.target.name] = event.target.value;
        console.log(newMovie);
        this.setState({
            newMovie: newMovie,
        });
    }

    componentDidMount() {
        this.setState({
            moviesSearch: this.props.movies,
        })
    }

    render() {
        return (
            <Row className="contentContainer">
                <Col>
                    <input className="searchBar" type="text" onChange={this.searchMovie} placeholder="Search a movie" />
                    <br/>
                    <br/>
                    <Table striped bordered hover responsive>
                        <thead>
                        <tr className="availableCol">
                            <th>Title</th>
                            <th>Director</th>
                            <th>Year</th>
                            <th>Narrative</th>
                            <th>Language</th>
                            <th>Available</th>
                            <th>Delete movie</th>
                        </tr>
                        </thead>
                        <tbody>
                        {
                            this.state.moviesSearch.map((movie, item)=>{
                                return <tr id={movie.title+"-"+item} key={movie.title+"-"+item}>
                                    <td>{movie.title}</td>
                                    <td>{movie.director}</td>
                                    <td>{movie.year}</td>
                                    <td>{movie.narrative}</td>
                                    <td>{movie.language}</td>
                                    <td className="availableCol">{movie.available? <i className="fa fa-check-circle"></i>:<i className="fa fa-times-circle"></i>}</td>
                                    <td className="availableCol">
                                        <button id={"button-"+item} onClick={this.deleteMovie} className="btn btn-light myShowButton">
                                            <i id={"icon-"+item} className="fa fa-trash"></i>
                                        </button>
                                    </td>
                                </tr>
                            })
                        }
                        </tbody>
                    </Table>
                    <Row>
                        <Col className="showButton">
                            <button onClick={this.props.showAdminView} className="btn btn-light myShowButton">
                                <i className="fa fa-chevron-left"></i> Back to main page
                            </button>
                        </Col>
                        <Col className="showButton">
                            <button onClick={this.handleShow} className="btn btn-light myShowButton">
                                <i className="fa fa-plus"></i> Create movie
                            </button>
                        </Col>
                    </Row>
                    <Modal show={this.state.showMovieModal} onHide={this.handleClose}>
                        <Modal.Header closeButton>
                            <Modal.Title>Create Movie</Modal.Title>
                        </Modal.Header>
                        <Modal.Body>
                            <Form>
                                <Form.Group controlId="name">
                                    <Form.Label>Name</Form.Label>
                                    <Form.Control placeholder="My movie" name="title" onChange={this.onChange} />
                                </Form.Group>

                                <Form.Group controlId="director">
                                    <Form.Label>Director</Form.Label>
                                    <Form.Control placeholder="Max Mustermann" name="director" onChange={this.onChange} />
                                </Form.Group>

                                <Form.Group controlId="language">
                                    <Form.Label>Language</Form.Label>
                                    <Form.Control as="select" name="language" onChange={this.onChange} >
                                        <option></option>
                                        <option>English</option>
                                        <option>Spanish</option>
                                        <option>German</option>
                                        <option>Portuguese</option>
                                        <option>Italian</option>
                                        <option>Latin</option>
                                        <option>French</option>
                                    </Form.Control>
                                </Form.Group>

                                <Form.Row>
                                    <Form.Group as={Col} controlId="narrative">
                                        <Form.Label>Narrative</Form.Label>
                                        <Form.Control as="select" name="narrative" onChange={this.onChange} >
                                            <option></option>
                                            <option>Thriller</option>
                                            <option>Science Fiction</option>
                                            <option>Psichological Trhiller</option>
                                            <option>Drama</option>
                                            <option>Love</option>
                                            <option>Comedy</option>
                                            <option>Action</option>
                                            <option>Animated</option>
                                            <option>Anime</option>
                                            <option>Documentary</option>
                                        </Form.Control>
                                    </Form.Group>

                                    <Form.Group as={Col} controlId="year">
                                        <Form.Label>Year</Form.Label>
                                        <br/>
                                        <input className="numberReact" type="number" min="1700" name="year" onChange={this.onChange} />
                                    </Form.Group>
                                </Form.Row>
                                <Modal.Footer>
                                    <Button variant="primary" onClick={this.createMovie}>
                                        Submit
                                    </Button>
                                    <Button variant="danger" onClick={this.handleShow}>
                                        Close
                                    </Button>
                                </Modal.Footer>
                            </Form>
                        </Modal.Body>
                    </Modal>
                </Col>
            </Row>
        );
    }
}