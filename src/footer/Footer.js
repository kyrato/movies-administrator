import React from 'react';
import { Row, Col } from "react-bootstrap"
import '../App.css';

export default class Footer extends React.Component {

    constructor(props, context) {
        super(props, context);

        /**
         * Component states
         * @type {{show: boolean}}
         */
        this.state = {
        };
    }

    render() {
        return (
            <Row className="footer">
                <Col className="showButton">
                    <ul className="footerList">
                        <li><b>Author: </b>Cesar Quintero Arias</li>
                        <li><b>Contact: </b>+49 1767 3411 597</li>
                        <li><b>E-Mail: </b>cequinteroar@unal.edu.co</li>
                        <li><b>Expected Job Position: </b>Software Developer - FrontEnd Developer</li>
                        <li><b>Framework used: </b>React JS</li>
                    </ul>
                </Col>
                <Col className="showButton">
                    <ul className="footerList">
                        <li><b>Type of application: </b>Single-page application (SPA)</li>
                        <li><b>Powered by: </b>React, Javascript, React Bootstrap, HTML5, CSS, Font awesome</li>
                    </ul>
                </Col>
            </Row>
        );
    }
}