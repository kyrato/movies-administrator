import React from 'react';
import {Button, Row, Col, Modal } from "react-bootstrap"
import '../App.css';

export default class Overview extends React.Component {

    constructor(props, context) {
        super(props, context);

        this.handleShow = this.handleShow.bind(this);
        this.handleClose = this.handleClose.bind(this);

        /**
         * Component states
         * @type {{show: boolean}}
         */
        this.state = {
            show: false,
        };
    }

    /**
     * Show Modal
     */
    handleShow(){
        this.setState({
            show: !this.state.show,
        });
        // console.log(this.state.show);
    }

    /**
     * Hide Modal
     */
    handleClose(){
        this.setState({
            show: !this.state.show,
        });
        // console.log(this.state.show);
    }

    render() {
        const moviesNum = this.props.movies.length;
        return (
            <Row className="contentContainer">
                <Col>
                    <h1 className="titles"><b>FRONT END EXERCISE</b></h1>
                    <h1>Create a webpage to create / delete movies</h1>
                    <h2>Requirements:</h2>
                    <p>Create a webpage that contains the following sections:</p>
                    <ul>
                        <li>An overview page with the description of the exercise, the author and the number of movies that have
                            already been created.</li>
                        <li>A page that shows the list of created movies. A movie is defined by its title.</li>
                        <li>It should be possible to add new movies or delete existing ones.</li>
                        <li>The page should allow to filter the existing movies with a search bar.</li>
                    </ul>
                    <h2>Acceptance Criteria:</h2>
                    <p>The candidate should design and create a Vue project that meets the requirements described above.
                        It will not only valued a working exercise but also:</p>
                    <ul>
                        <li>How the components are organized and code quality.</li>
                        <li>Used frameworks.</li>
                        <li>How in memory data is handled</li>
                        <li>A single unit test for a feature that the candidate consider. It is not required to do unit tests for all the
                            features.</li>
                    </ul>
                    <h2>Clarifications:</h2>
                    <ul>
                        <li>It is not expected to have a real connection with any backend.</li>
                        <li>It is not expected to persist the movies when the page is fully refreshed.</li>
                        <li>It is not expected to use any pagination technique.</li>
                        <li>It is not expected to design the frontend for a mobile device, target device will be a 13 inches or higher
                            laptop.</li>
                    </ul>
                    <Row>
                        <Col className="showButton">
                            <button onClick={this.handleShow} className="btn btn-light myShowButton">
                                <i className="fa fa-eye"></i> Show created movies
                            </button>
                        </Col>
                        <Col className="showButton">
                            <button onClick={this.props.showAdminView} className="btn btn-light myShowButton">
                                <i className="fa fa-database"></i> Administrate movies
                            </button>
                        </Col>
                    </Row>
                    <Modal show={this.state.show} onHide={this.handleClose}>
                        <Modal.Header closeButton>
                            <Modal.Title>Created Movies</Modal.Title>
                        </Modal.Header>
                        <Modal.Body>At this moment, <b className="moviesNum">{moviesNum}</b> movies have been created</Modal.Body>
                        <Modal.Footer>
                            <Button variant="danger" onClick={this.handleShow}>
                                Close
                            </Button>
                        </Modal.Footer>
                    </Modal>
                </Col>
            </Row>
        );
    }
}